<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en" hola_ext_inject="disabled">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mauricio El Uri</title>
        <link href="<?echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
        <link href="<?echo base_url('assets/css/signin.css');?>" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <form class="form-signin" action="<?echo base_url('conta/entrar');?>" method="POST">
                <input type="hidden" name="captcha">
                <h2 class="form-signin-heading">Login</h2>
                <input type="text" id="inputEmail" class="form-control" placeholder="Nome" name="nome" maxlength="20" pattern=".{6,}" title=" Mínimo de 6 caracteres" required autofocus>
                <input type="password" id="inputPassword" class="form-control" placeholder="Senha" maxlength="20" name="senha" pattern=".{6,}" title=" Mínimo de 6 caracteres" required>
                <?php
                if ($alerta != NULL) {
                    echo "<div class='alert alert-danger " . $alerta["class"] . "'>" . $alerta["mensagem"] . "</div>";
                }
                ?>
                <button class="btn btn-lg btn-primary btn-block" type="submit" name="entrar" value="entrar">Entrar</button>
            </form>
        </div>
    </body>
</html>