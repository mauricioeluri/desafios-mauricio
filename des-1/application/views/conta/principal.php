<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en" hola_ext_inject="disabled">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mauricio El Uri</title>
        <link href="<?echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
        <link href="<?echo base_url('assets/css/signin.css');?>" rel="stylesheet">
        <script>
            function entrar() {
                window.open("<?echo base_url('conta/entrar');?>", "_self");
            }
            function criar() {
                window.open("<?echo base_url('conta/criar');?>", "_self");
            }
        </script>
    </head>
    <body>
        <div class="container">
            <form class="form-signin" action="<?echo base_url('conta/entrar');?>" method="POST">
                <input type="hidden" name="captcha">
                <h2 class="form-signin-heading">Escolha sua opção:</h2>
                <input type="button" class="btn btn-lg btn-primary btn-block" value="Entrar" onclick="entrar();">
                <input type="button"class="btn btn-lg btn-success btn-block" value="Criar conta" onclick="criar();">
            </form>
        </div>
    </body>
</html>