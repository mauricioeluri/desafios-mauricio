<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
$email = $this->session->userdata('mail');
?>
<!DOCTYPE html>
<html lang="en" hola_ext_inject="disabled">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mauricio El Uri</title>
        <link href="<?echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
        <link href="<?echo base_url('assets/css/signin.css');?>" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <form class="form-signin" action="<?echo base_url('conta/modificarSalvar');?>" method="POST">


                <input type="hidden" name="captcha">
                <h2 class="form-signin-heading">Modificar conta</h2>
                <input type="text" id="inputNome" class="form-control" placeholder="Nome" value="<? echo $email; ?>" name="nome" maxlength="20" pattern=".{6,}" title=" Mínimo de 6 caracteres" required autofocus>
                <input type="password" id="inputPassword" class="form-control" placeholder="Altere ou confirme a senha" maxlength="20"  pattern=".{6,}" title=" Mínimo de 6 caracteres" name="senha" required>
                <div class="checkbox">
                    <label><input type="checkbox" id="inputAdmin" name="admin" value=""/> Admin?</label>
                </div>
                <button class="btn btn-lg btn-success btn-block" type="submit" name="entrar" value="entrar">Modificar Conta</button>
            </form>
        </div>
    </body>
</html>