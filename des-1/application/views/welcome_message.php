<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$logado = $this->session->userdata('logado');

if ($logado === TRUE) {
    $email = $this->session->userdata('mail');
    $txt1 = base_url('conta/sair') . "'>Sair</a>";
    //$usr = $this->session->userdata('teste');
    //$txt2 = $usr;
} else {
    redirect(base_url('conta/escolha'));
    $email = 'CodeIgniter';
    $txt1 = base_url('conta/entrar') . "'>Entrar</a>";
}
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Welcome</title>

        <link href="<?echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">


    </head>
    <body>

        <div id="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <? echo "<h1>Welcome to $email</h1><a href='$txt1"; ?><br>
                    <a href="<? echo base_url('conta/modificar'); ?>">Modificar conta</a><br>
                    <a href="<? echo base_url('conta/deletar'); ?>">Deletar conta</a>
                    <br><br><br>

                    <?php echo "Tabela"; ?>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Senha</th>
                                <th>Admin</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info">
                                <?php
                                foreach ($usuarios as $dados) {
                                    echo "<td>" . $dados->id . "</td><td>" . $dados->nome . "</td><td>" . $dados->senha . "</td><td>" . $dados->admin . "</td>";
                                    if ($dados->admin == 1) {
                                        $msg = "<h1>Parabéns, você é um administrador!!!</h1>";
                                    } else {
                                        $msg = "<h1>Você é um usuário comum. :/</h1>";
                                    }

                                    echo $msg;
                                }

//                                adminAcess($admin);
                                ?>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</body>
</html>