<?php

//|valid_email');
defined('BASEPATH') OR exit('No direct script access allowed');

class Conta extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('usuarios_model');
    }

    public function entrar() {
        $alerta = NULL;

        if ($this->input->post('entrar') === 'entrar') {
            if ($this->input->post('captcha'))
                redirect('conta/entrar');

            $this->form_validation->set_rules('nome', 'NOME', 'required'); //|valid_email');
            $this->form_validation->set_rules('senha', 'SENHA', 'required|min_length[6]|max_length[20]');

            if ($this->form_validation->run() === TRUE) {

                $email = $this->input->post('nome');
                $senha = $this->input->post('senha');
                $senhaH = md5($senha);
                $login_existe = $this->usuarios_model->check_login($email, $senhaH);

                if ($login_existe) {

                    $id = $this->usuarios_model->getId($login_existe);

                    $this->usuarios_model->setSession($id, $email);

                    redirect("welcome");
                } else {
                    $alerta = array(
                        "class" => "danger",
                        "mensagem" => "Login inválido!<br>Senha ou e-mail incorretos!!<br>" . validation_errors()
                    );
                }
            } else {
                $alerta = array(
                    "class" => "danger",
                    "mensagem" => "Atenção! Falha na validação do formulário!!<br>" . validation_errors()
                );
            }
        }
        $dados = array(
            "alerta" => $alerta
        );
        $this->load->view('conta/entrar', $dados);
    }

    public function adicionar() {

        $alerta = NULL;

        if ($this->input->post('entrar') === 'entrar') {
            $this->form_validation->set_rules('nome', 'NOME', 'required|min_length[6]|max_length[20]');
            $this->form_validation->set_rules('senha', 'SENHA', 'required|min_length[6]|max_length[20]');

            if ($this->form_validation->run() === TRUE) {

                $nome = $this->input->post('nome');
                $senha = $this->input->post('senha');
                $senhaH = md5($senha);
                $admin = $this->input->post('admin');

                if (isset($admin) == 1) {
                    $admin = 1;
                } else {
                    $admin = 0;
                }
                $table = array(
                    'nome' => $nome,
                    'senha' => $senhaH,
                    'admin' => $admin
                );
                $this->usuarios_model->insereUsuario($table);
                if ($table) {
                    $usuario = $this->usuarios_model->check_login($nome, $senhaH);
                    $id = $this->usuarios_model->getId($usuario);
                    $this->usuarios_model->setSession($id, $nome);
                    redirect("welcome");
                } else {
                    $alerta = array(
                        "class" => "danger",
                        "mensagem" => "Login inválido!<br>Senha ou e-mail incorretos!!<br>" . validation_errors()
                    );
                }
            } else {
                $alerta = array(
                    "class" => "danger",
                    "mensagem" => "Atenção! Falha na validação do formulário!!<br>" . validation_errors()
                );
            }
        }
        $dados = array(
            "alerta" => $alerta
        );
        $this->load->view('conta/criar', $dados);
    }

    public function deletar() {
        $id = $this->session->userdata('id');
        $this->usuarios_model->deletaUsuario($id);
        $this->sair();
    }

    public function sair() {
        $this->session->sess_destroy();
        redirect('conta/escolha');
    }

    public function escolha() {
        $this->load->view('conta/principal');
    }

    public function criar() {
        $this->load->view('conta/criar');
    }

    public function modificar() {
        $sess = $this->session->userdata('logado');
        if ($sess == TRUE) {
            $this->load->view('conta/modificar');
        } else {
            redirect('conta/escolha');
        }
    }

    public function modificarSalvar() {

        $this->form_validation->set_rules('nome', 'NOME', 'required|min_length[6]|max_length[20]');
        $this->form_validation->set_rules('senha', 'SENHA', 'required|min_length[6]|max_length[20]');

        if ($this->form_validation->run() === TRUE) {

            $nome = $this->input->post('nome');
            $senha = $this->input->post('senha');
            $senhaH = md5($senha);
            $admin = $this->input->post('admin');

            if (isset($admin) == 1) {
                $admin = 1;
            } else {
                $admin = 0;
            }

            $id = $this->session->userdata('id');

            $table = array(
                'id' => $id,
                'nome' => $nome,
                'senha' => $senhaH,
                'admin' => $admin
            );

            if ($table) {
                $this->usuarios_model->alteraUsuario($id, $table);
                $usuario = $this->usuarios_model->getDadosOK($id);
                $email = $usuario->row()->nome;
                $this->usuarios_model->setSession($id, $email);

                redirect("welcome");
            } else {
//login erro
                $alerta = array(
                    "class" => "danger",
                    "mensagem" => "Login inválido!<br>Senha ou e-mail incorretos!!<br>" . validation_errors()
                );
            }
        } else {
            $alerta = array(
                "class" => "danger",
                "mensagem" => "Atenção! Falha na validação do formulário!!<br>" . validation_errors()
            );
        }

        $dados = array(
            "alerta" => $alerta
        );

        $this->load->view('conta/criar', $dados);
    }

}
