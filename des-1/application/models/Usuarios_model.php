<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

    public function check_login($email, $senha) {
        $this->db->where('nome', $email);
        $this->db->where('senha', $senha);
        $usuario = $this->db->get('usuario');
        if ($usuario->num_rows() > 0) {
            return $usuario;
        } else {
            return null;
        }
    }

    function getId($usuario) {
        $id = $usuario->row()->id;
        return $id;
    }

    public function setSession($id, $email) {
        $session = array(
            'id' => $id,
            'mail' => $email,
            'logado' => TRUE,
        );
        $this->session->set_userdata($session);
    }

    public function getDados($id) {
        $usuario = $this->db->where('id', $id);
        $usuario = $this->db->get('usuario');
        return $usuario->result();
        //return $usuario;
    }
    public function getDadosOK($id) {
        $usuario = $this->db->where('id', $id);
        $usuario = $this->db->get('usuario');
        return $usuario;
    }

    public function adminAcess($admin) {
        if ($admin == 1) {
            echo '<h3>FUNÇÃO ADMIN - SOMENTE UM ADMINISTRADOR PODE VER ESSA MENSAGEM.<br>PARABÉNS!!!!<h3>';
        } else {
            return FALSE;
        }
    }

    public function insereUsuario($table) {
        $query = $this->db->insert('usuario', $table);
        return true;
    }

    public function alteraUsuario($id, $table) {

        $query = $this->db->where('id', $id);
        $query = $this->db->update('usuario', $table);
       return true;
    }


    public function deletaUsuario($id) {
        $this->db->delete('usuario', array('id' => $id));
    }

}
